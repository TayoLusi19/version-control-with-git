# Exercise 13: Bugfix Workflow with Git

This exercise demonstrates how to efficiently manage and fix bugs in your project using Git. You'll create a bugfix branch, correct a spelling error in your code, and push the changes back to the remote repository.

## Steps to Fix the Bug

### Step 1: Create a Bugfix Branch

Begin by creating a branch dedicated to your bugfix. This isolates your work and ensures that fixes can be reviewed separately from ongoing development work.

```bash
git checkout -b bugfix/changes
```
### Step 2: Fix the Spelling Error

Open the **Application.java** file located in **src/main/java/com.** Navigate to line 22 and correct the spelling error in the log message to ensure it reads correctly:

```bash
log.info("Java app started");
```
### Step 3: Review Your Changes

Before committing, it's important to review your changes. Use git diff to display the differences between your working directory and the index (staged files), helping you catch any unintended changes.

```bash
git diff
```

### Step 4: Commit Your Changes

Once you're satisfied with the fix, add your changes to the staging area and commit them with a clear, descriptive message. This message should briefly describe the bug fix for future reference.

```bash
git add .
git commit -m "Fix spelling error in Application.java"
```
### Step 5: Push Changes to Remote

Finally, push your bugfix branch and its commits to the remote repository. This makes your changes available for review and integration into the main project.

```bash
git push --set-upstream origin bugfix/changes
```

## Best Practices
- Branch Naming: Use descriptive names for your branches that reflect their purpose, such as bugfix/description-of-the-fix.
- Commit Messages: Write clear, concise commit messages that directly state what has been fixed or changed.
- Review Before Committing: Always review your changes with git diff to avoid committing unintended modifications.

By following these steps, you ensure that bugs are fixed in a structured manner, improving the quality and reliability of your project.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/version-control-with-git/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Led a bugfix on bugfix/changes branch, correcting a spelling error in Application.java and ensuring code integrity through review and push to the remote repository.

## Resume Bullet Point

- Corrected a critical spelling error in Application.java via bugfix/changes branch, enhancing code quality and documentation before pushing updates to the remote repository.
