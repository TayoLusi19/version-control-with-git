# Exercise 15: Fix Merge Conflict

In this exercise, you'll learn how to update a dependency within a bugfix branch, merge changes from the master branch to avoid conflicts, and resolve any merge conflicts that arise during the process.

## Steps to Update and Merge

### Step 1: Switch to the Bugfix Branch
Ensure you're working on the correct branch by switching to your bugfix branch:

```bash
git checkout bugfix/changes
```

### Step 2: Update the Logger Library Version
Locate the logstash-logback-encoder dependency in the build.gradle file (line 18) and update its version from 5.2 to 7.2:

```bash
compile group: 'net.logstash.logback', name: 'logstash-logback-encoder', version: '7.2'
```

### Step 3: Commit the Change Locally
After updating the version, commit your change to the bugfix branch:

```bash
git add .
git commit -m "upgrade logger library version"
```

Step 4: Merge Master into Bugfix Branch
To ensure your bugfix branch is up-to-date with the master branch and to minimize conflicts:

```bash
git merge master
```

### Step 5: Resolve Any Merge Conflicts
If you encounter a merge conflict, especially in the **build.gradle** file regarding the logger library version:

**Open the conflicted file** and manually edit it to resolve the conflict by choosing the correct version or making the necessary adjustments.
**Use git status** to review which files were conflicted and ensure all conflicts have been resolved.

### Step 6: Commit and Push the Merge
Once all conflicts are resolved:

```bash
git add .
git commit -m "Resolved merge conflict by updating logger library version"
git push origin bugfix/changes
```
## Best Practices
- Regularly update your feature and bugfix branches with changes from the master branch to minimize merge conflicts.
- Carefully review and resolve merge conflicts to ensure that the changes are correctly integrated.
- Communicate with your team when merging significant changes to avoid duplicating efforts or creating additional conflicts.
By following these steps, you ensure smooth integration of updates and maintain the integrity of your project's codebase.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/version-control-with-git/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Resolved merge conflicts in bugfix/changes branch and updated logger library, ensuring code consistency with the master branch.
