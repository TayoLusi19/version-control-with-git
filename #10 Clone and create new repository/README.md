# Exercise 10: Clone and Create New Repository

This exercise guides you through the process of cloning a Git repository, creating a new local copy, and pushing it to your own GitLab remote repository. Follow the steps below to complete the exercise.

## Prerequisites

- A GitLab account
- Git installed on your local machine

## Instructions

### Step 1: Clone Repository and Change Directory

First, clone the repository and change into the project directory using the following commands:

```bash
git clone git@gitlab.com:TayoLusi19/git-exercises.git
cd git-exercises
```

### Step 2: Remove Remote Repo Reference and Create Your Own Local Repository

Remove the existing **.git** directory to disassociate from the original remote repository. Then, initialize a new Git repository, add all files, and make your first commit:

```bash
rm -rf .git
git init
git add .
git commit -m "Initial commit"
```

### Step 3: Create Git Repository on GitLab and Push

Before proceeding, ensure you have created a new repository on GitLab through the GitLab UI. Replace {gitlab-user} and {gitlab-repo} with your GitLab username and the new repository name, respectively. Then, execute the following commands to add your GitLab repository as a remote and push your local repository to it:

```bash
git remote add origin git@gitlab.com:{gitlab-user}/{gitlab-repo}.git
git push -u origin master
```

## Completion 

Congratulations! You have successfully cloned a repository, prepared a local version, and pushed it to your GitLab account. This exercise is fundamental for understanding how to manage and share code using Git and GitLab.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/version-control-with-git/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point


- Cloned and detached a GitLab repository, initializing a new local Git version and pushing to a newly created GitLab repository.
- Demonstrated Git proficiency by managing remote repository settings and executing initial push operations.
