# Exercise 11: Setting up `.gitignore` to Exclude Unwanted Files

When working with Git repositories, it's a common best practice to ignore certain files and directories that do not need to be tracked. These often include build directories, editor configuration folders, and system-specific files like `.DS_Store` on macOS. This exercise guides you through ignoring such files and directories in a Git repository and cleaning up the repository by removing the already tracked instances of these ignored files.

## Objectives

- Create a `.gitignore` file to specify the files and directories Git should ignore.
- Remove cached versions of these files and directories from the Git repository if they were previously tracked.

## Steps to Complete the Exercise

### 1. Create the `.gitignore` File

Start by creating a `.gitignore` file in the root directory of your repository. Add the following entries to ignore the `.idea` folder (used by JetBrains IDEs), `.DS_Store` (a macOS system file), and the `out` and `build` directories often used for compiled or generated files.

```plaintext
.idea
.DS_Store
out
build
```

### 2. Remove Cached Files

If any of these files or directories were previously tracked by Git, you need to remove them from the Git cache while keeping them in your local filesystem. Use the following commands to do so:

```bash
git rm --cached .DS_Store
git rm -r --cached .idea
git rm -r --cached out
git rm -r --cached build
```

The -r flag is used to recursively remove directories.

### 3. Commit and Push .gitignore and Changes

After updating the Git cache, add the .gitignore file to your repository, commit the changes, and push them to the remote repository:

```bash
git add .gitignore
git commit -m "Added .gitignore and removed ignored files from tracking"
git push
```

### Notes
- The git rm --cached command only removes files from tracking; it does not delete them from your local filesystem.

- It's a good idea to run git status before and after these operations to ensure that the correct changes are being made.

- If other contributors are working on the same repository, inform them of these changes. They might need to pull the latest changes and may also need to perform some cleanup in their local clones if they have local changes to the ignored files.
By following these steps, you ensure that your repository is clean and only tracks relevant files, avoiding clutter and potential conflicts related to system-specific or build-generated files.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/version-control-with-git/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Implemented and managed .gitignore to exclude irrelevant files (like .DS_Store, .idea, and build directories) from Git tracking, enhancing repository cleanliness and efficiency.
