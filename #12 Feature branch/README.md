# Exercise 12: Creating a Feature Branch and Making Changes

This exercise walks you through the process of creating a feature branch in your Git repository, making changes to upgrade a dependency and add an image to a webpage, and then pushing these changes to your remote repository.

## Objectives

- Create a feature branch.
- Make specific changes to the project files.
- Check the changes with `git diff`.
- Commit the changes with a descriptive commit message.
- Push the changes to the remote repository.

## Steps to Complete the Exercise

### 1. Create a Feature Branch

Create a new branch for your feature work. This isolates your changes and makes it easier to review and merge them later.

```bash
git checkout -b feature/changes
```
### 2. Make the Required Changes

Upgrade Dependency
In the **build.gradle** file, locate the **logstash-logback-encoder** dependency on line 18 and update its version from **5.2** to **7.3**.

```bash
compile group: 'net.logstash.logback', name: 'logstash-logback-encoder', version: '7.3'
```

Add Image to **index.html**
Find the **index.html** file in the **src/main/webapp** folder. On line 9, add the following HTML to include the specified image:

```bash
<img src="https://www.careeraddict.com/uploads/article/58721/illustration-group-people-team-meeting.jpg" alt="Team Meeting Illustration" />
```

### 3. Check Your Changes

Before committing your changes, it's a good practice to review them using **git diff**. This command shows the differences between the working directory and the index, allowing you to verify the changes you made.

```bash
git diff
```

### 4. Commit the Changes

If you're satisfied with the changes, add them to the staging area and commit them with a descriptive message. The message should clearly describe what changes were made and why.

```bash
git add .
git commit -m "Upgrade logback library and add team meeting image to index.html"
```
### 5. Push Your Changes to the Remote Repository

Finally, push your feature branch and its commits to the remote repository.

```bash
git push --set-upstream origin feature/changes
```
## Notes
Always check with your team's naming conventions for branches and commit messages to maintain consistency across the project.
Reviewing changes with git diff before committing is a good practice to catch any unintended changes or errors.
By following these steps, you have successfully created a feature branch, made necessary changes to your project, and pushed these changes to your remote repository, ready for review and integration into the main project.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/version-control-with-git/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Managed feature/changes branch, updating dependencies and adding web content, with careful review and push to remote.
