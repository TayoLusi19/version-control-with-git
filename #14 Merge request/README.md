# Exercise 14: Merge Request Workflow

After completing your work on a feature branch, the next step is to merge these changes into the master branch for testing and deployment. This exercise guides you through the process of creating a merge request to merge your feature branch into the master branch.

## Steps for Merging a Feature Branch into Master

### Using GitLab UI for Merge Request

The preferred method for merging feature branches is through a merge request in GitLab, as it facilitates code review and discussions before the merge.

1. **Push Your Feature Branch:** Ensure your feature branch is up to date with all changes pushed to the remote repository.
```bash
git push origin feature/changes
```
### Create a Merge Request:

1. Go to your project in GitLab.
2. Navigate to 'Merge Requests' and click 'New merge request'.
3. Select your feature branch (feature/changes) as the source branch and master as the target branch.
4. Fill in the merge request details, assign reviewers if required, and submit the merge request.

### Review and Merge:

- Once the merge request is reviewed and approved by your team or CI/CD pipeline checks pass, you can merge the feature branch into the master branch directly from GitLab UI by clicking on 'Merge'.

## Merging Locally and Pushing to Remote Master

If necessary, or for small changes that don't require a review process, you can merge the feature branch into the master branch locally and then push the changes. However, this method is less preferred due to bypassing the review process.

### 1. Switch to the Master Branch:
```bash
git checkout master
```

### 2. Merge the Feature Branch into Master:
```bash
git merge feature/changes
```

### 3. Push the Merge to Remote Master:
```bash
git push origin master
```

## Best Practices
- Merge Requests: Always prefer using merge requests for merging feature branches. This allows for code reviews, discussions, and better tracking of changes.
- Testing: Ensure that your feature branch is thoroughly tested before initiating a merge request to avoid integration issues.
- Conflict Resolution: Handle any merge conflicts that arise during the merge process promptly and carefully to maintain code integrity.
By following these steps, you maintain a clean, organized repository and ensure that new features are integrated smoothly into your project.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/version-control-with-git/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Facilitated feature integration via GitLab merge requests, ensuring thorough reviews and testing before merging feature/changes into the master branch for deployment.
