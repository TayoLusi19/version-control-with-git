# Exercise 16: Revert Commit in Git

This exercise covers the process of making and pushing commits to a bugfix branch, including fixing a spelling mistake and updating an image URL in the `index.html` file, and then reverting a commit after it's been pushed to the remote repository.

## Steps to Fix, Commit, Push, and Revert

### Fixing a Spelling Mistake

1. **On your bugfix branch, locate the `index.html` file** in the `src/main/webapp` directory. Find the line with the spelling error (line 11) and correct it:

    ```html
    <li>Sarah - Full stack developer</li>
    ```

2. **Commit the change locally:**

    ```bash
    git add .
    git commit -m "Fix spelling error"
    ```

### Updating the Image URL

3. **In the same `index.html` file, update the image URL** on line 9:

    ```html
    <img src="https://www.tameday.com/wp-content/uploads/2018/10/effective-meetings.jpg" width="" />
    ```

4. **Commit this change locally as well:**

    ```bash
    git add .
    git commit -m "Set new image url"
    ```

### Pushing the Commits

5. **Push both commits to the remote repository:**

    ```bash
    git push
    ```

### Reverting the Last Commit

6. **If you need to revert the last commit** (e.g., the image URL change was incorrect), you can use `git revert`:

    ```bash
    git revert HEAD
    git push
    ```

    This command creates a new commit that undoes the changes made by the last commit and pushes the reversion to the remote repository.

## Best Practices

- **Commit Related Changes Separately:** Group related changes into individual commits to make it easier to review changes and revert specific changes if necessary.
- **Use Descriptive Commit Messages:** Clearly describe what each commit does to make it easier to understand the history and purpose of changes.
- **Revert with Caution:** Reverting a pushed commit should be done with caution, especially in shared branches. Communicate with your team before reverting changes that have been pushed.

By following these steps, you can effectively manage changes and maintain the integrity of your project's codebase, even when needing to undo some of those changes.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/version-control-with-git/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Managed and reverted commits in index.html, showcasing proficiency in Git for project accuracy.
