# Version Control with Git



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/TayoLusi19/version-control-with-git.git
git branch -M main
git push -uf origin main
```

# Resume Bullet Point


## Exercises 10
- Cloned and detached a GitLab repository, initializing a new local Git version and pushing to a newly created GitLab repository.
- Demonstrated Git proficiency by managing remote repository settings and executing initial push operations.

## Exercises 11
- Implemented and managed .gitignore to exclude irrelevant files (like .DS_Store, .idea, and build directories) from Git tracking, enhancing repository cleanliness and efficiency.

## Exercises 12
- Managed feature/changes branch, updating dependencies and adding web content, with careful review and push to remote.

## Exercises 13
- Corrected a critical spelling error in Application.java via bugfix/changes branch, enhancing code quality and documentation before pushing updates to the remote repository.

## Exercises 14
- Facilitated feature integration via GitLab merge requests, ensuring thorough reviews and testing before merging feature/changes into the master branch for deployment.

## Exercises 15
- Resolved merge conflicts in bugfix/changes branch and updated logger library, ensuring code consistency with the master branch.

## Exercises 16
- Managed and reverted commits in index.html, showcasing proficiency in Git for project accuracy.

## Exercises 17
- Efficiently managed Git history by updating and resetting index.html commit, streamlining project workflow.

## Exercises 18
- Directly merged bugfix/changes into master, ensuring up-to-date integration and stability with careful conflict resolution and testing.

## Exercises 19
- Cleaned up repository by deleting merged bugfix/changes and feature/changes branches locally and remotely, maintaining a clutter-free project environment.
