# Exercise 17: Reset Commit

This exercise demonstrates how to undo a local commit in Git. You'll make a change to an `index.html` file to update a team member's role, commit that change, and then reset the commit after discovering the update has already been addressed in another branch.

## Steps to Update Text, Commit, and Reset

### Updating the Team Member's Role

1. **While on your bugfix branch, locate the `index.html` file** in the `src/main/webapp` directory. Find the line that mentions Bruno's role (line 15) and update it to reflect his move to the DevOps team:

    ```html
    <li>Bruno - DevOps engineer</li>
    ```

### Committing the Change Locally

2. **Commit this change locally:**

    ```bash
    git add .
    git commit -m "Adjust Bruno's role description"
    ```

### Resetting the Last Local Commit

3. **If you need to undo this local commit** (e.g., the change has already been made in another branch), you can reset to the previous commit. This command undoes the last commit and any changes it contained:

    ```bash
    git reset --hard HEAD~
    ```

    This action will reset your current branch's HEAD to the previous commit, effectively removing the last commit and its changes from your local history.

## Best Practices

- **Commit with Intent:** Before committing changes, ensure they are necessary and not duplicated in other branches.
- **Communicate with Your Team:** Before resetting commits, especially if you're considering pushing afterwards, ensure the changes are indeed unnecessary or addressed elsewhere.
- **Use Reset Carefully:** `git reset --hard` will discard all changes in your working directory and index. If you have uncommitted changes you want to keep, consider using `git reset --soft` or `git reset --mixed`.

By following these steps, you can manage your local Git history effectively, ensuring that only necessary changes are committed and maintaining a clean, accurate project history.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/version-control-with-git/-/blob/main/LICENSE.md?ref_type=heads)

# Author
Adetayo Michael Ibijemilusi

## Resume Bullet Point

- Efficiently managed Git history by updating and resetting index.html commit, streamlining project workflow.
